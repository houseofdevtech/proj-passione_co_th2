$(function () {
    $('.list-directory').listnav({
        includeNums: false,
        showCounts: false,
    });
    const ps = new PerfectScrollbar('.list-directory', {
        wheelPropagation: true
    });
})